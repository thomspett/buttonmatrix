#ifndef ButtonMatrix_h
#define ButtonMatrix_h

#include "Arduino.h"
#include <DcsBios.h>

using namespace DcsBios;

typedef struct {
  unsigned char current : 1, last : 1, changed: 1;
} buttonState_t;

class ButtonMatrix : PollingInput {
  private:
    void pollInput();
    void _driveScanLine(int n);
    void _readCodeAtScanLine(int i);
    int _scanDepth;
    int _codeWidth;
    int* _scanPins;
    int* _codePins;
    buttonState_t* _buttonStates;
    char** _messages;
    char** _args_on;
    char** _args_off;
    int _numButtons;
  public:
    ButtonMatrix(char** msgs, char** args_on, char** args_off, 
        int scanDepth, int codeWidth, int* scanPins, int* codePins);
    void readState();
    void serialize();
};


#endif
