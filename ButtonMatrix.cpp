#include "Arduino.h"
#include "ButtonMatrix.h"
#include <DcsBios.h>

  
ButtonMatrix::ButtonMatrix(
  char** messages, char** args_on, char** args_off,
  int scanDepth, int codeWidth, int* scanPins, int* codePins)
{
  _messages = messages;
  _args_on = args_on;
  _args_off = args_off;
  _scanDepth = scanDepth;
  _codeWidth = codeWidth;
  _scanPins = scanPins;
  _codePins = codePins;
  _numButtons = _scanDepth * _codeWidth;
  _buttonStates = new buttonState_t[_numButtons];
  
  //Enable pull-ups to avoid floating inputs
  for (int i=0; i < _codeWidth; i++)
  {
    pinMode(codePins[i], INPUT_PULLUP);
  }
}

void ButtonMatrix::pollInput()
{
  readState();
  for (int i=0; i < _numButtons; i++) {
    buttonState_t* button = &_buttonStates[i];
    if (button->changed == 1) {
      char * message = _messages[i];
      if (message && message[0] != '\0') {
        if (button->current == 1 && button->last == 0){
          sendDcsBiosMessage(_messages[i], _args_on[i]);
        }
        if (button->current == 0 && button->last == 1){
          sendDcsBiosMessage(_messages[i], _args_off[i]);
        }
      }
    }
  }
}

void ButtonMatrix::readState()
{
  for (int i=0; i < _scanDepth; i++)
  {
    _driveScanLine(i);
    _readCodeAtScanLine(i);
  }
}

void ButtonMatrix::serialize()
{
  for (int i=0; i < _numButtons; i++)
  {
    Serial.print(_buttonStates[i].current);
    Serial.print(_buttonStates[i].last);
    Serial.print(_buttonStates[i].changed);
  }
  Serial.println("");
}

void ButtonMatrix::_readCodeAtScanLine(int i)
{
  for (int j=0; j < _codeWidth; j++) {
    int index = i* _scanDepth + j;
    buttonState_t* button = &_buttonStates[index];
    button->last = button->current;
    button->current = !digitalRead(_codePins[j]);
    button->changed = (!button->current != !button->last);
  }
}  

void ButtonMatrix::_driveScanLine(int n)
{
  for (int j=0; j<_scanDepth; j++) {
    if (j==n) {
      pinMode(_scanPins[j], OUTPUT);
      //digitalWrite(_scanPins[j], LOW);
    } else {
      //digitalWrite(_scanPins[j], HIGH);
      //High-Z:
      pinMode(_scanPins[j], INPUT);
    }
  }
}
