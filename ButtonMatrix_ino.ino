#include "ButtonMatrix.h"
#include <DcsBios.h>

#define SCANDEPTH 4
#define CODEWIDTH 4
#define NONE ""
#define UFC_MC "UFC_MASTER_CAUTION"

DcsBios::ProtocolParser parser;

int scanDepth = SCANDEPTH;
int codeWidth = CODEWIDTH;
int scanPins[SCANDEPTH] = {22,23,24,25};
int codePins[CODEWIDTH] = {26,27,28,29};
char * messages[SCANDEPTH*CODEWIDTH] = {
  UFC_MC,       "UFC_1", "UFC_2", "UFC_3",
  "UFC_STEER", "UFC_4",    "UFC_5",    "UFC_6",
  "UFC_STEER", "UFC_7",    "UFC_8",    "UFC_9",
  NONE,        "UFC_SPACE",    "UFC_10",    "UFC_HACK"
};
char * args_on[SCANDEPTH*CODEWIDTH] = {
  "1", "1", "1", "1",
  "2", "1", "1", "1",
  "0", "1", "1", "1",
  "0", "1", "1", "1"
};
char * args_off[SCANDEPTH*CODEWIDTH] = {
  "0", "0", "0", "0",
  "1", "0", "0", "0",
  "1", "0", "0", "0",
  "0", "0", "0", "0"
};
ButtonMatrix bm(messages, args_on, args_off, 
  scanDepth, codeWidth, scanPins, codePins);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
}

void loop() {
  int data = Serial.read();
  while(data > -1) {
    parser.processChar(data);
    data = Serial.read();
  }
  DcsBios::PollingInput::pollInputs();
}

void sendDcsBiosMessage(const char* msg, const char* arg) {
  Serial.write(msg);
  Serial.write(' ');
  Serial.write(arg);
  Serial.write('\n');
}

void onDcsBiosWrite(unsigned int address, unsigned int value) {
  
}
